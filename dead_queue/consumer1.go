package main

import (
	"github.com/streadway/amqp"
	"log"
	"rabbit/utils"
)

const (
	NormalExchange = "normalExchange"
	DeadExchange   = "deadExchange"

	NormalQueue = "normalQueue"
	DeadQueue   = "deadQueue"
)

func main() {
	channel, _ := utils.GetRabbitMQChannel()

	_ = channel.ExchangeDeclare(NormalExchange, "direct", false, false, false, false, nil)
	_ = channel.ExchangeDeclare(DeadExchange, "direct", false, false, false, false, nil)

	args := amqp.Table{}
	args["x-dead-letter-exchange"] = DeadExchange
	args["x-dead-letter-routing-key"] = "deadMsg"
	//args["x-message-ttl"] = 10000
	//args["x-max-length"] = 10

	nqueue, _ := channel.QueueDeclare(NormalQueue, false, false, false, false, args)
	dqueue, _ := channel.QueueDeclare(DeadQueue, false, false, false, false, nil)

	_ = channel.QueueBind(nqueue.Name, "normalMsg", NormalExchange, false, nil)
	_ = channel.QueueBind(dqueue.Name, "deadMsg", DeadExchange, false, nil)

	msgs, _ := channel.Consume(nqueue.Name, "", false, false, false, false, nil)

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf(" [x] %s", d.Body)
			d.Reject(false)
		}
	}()

	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	<-forever
}
