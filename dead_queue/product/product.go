package main

import (
	"github.com/streadway/amqp"
	"rabbit/utils"
)

const (
	NormalExchange = "normalExchange"
)

func main() {

	channel, _ := utils.GetRabbitMQChannel()

	_ = channel.ExchangeDeclare(NormalExchange, "direct", false, false, false, false, nil)

	headers := make(amqp.Table)
	headers["x-delay"] = 10000

	_ = channel.Publish(NormalExchange, "normalMsg", false, false,
		amqp.Publishing{
			Headers:     headers,
			ContentType: "text/plain",
			Body:        []byte("hello rabbit"),
		})
}
