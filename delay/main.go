package main

import (
	"github.com/streadway/amqp"
	"rabbit/utils"
)

const (
	ExchangeName = "delay"
	QueueName    = "queue"
)

func init() {
	channel, _ := utils.GetRabbitMQChannel()

	table := amqp.Table{}
	table["x-delayed-type"] = "direct"

	_ = channel.ExchangeDeclare(ExchangeName, "x-delayed-message", false, false, false, false, table)

	queue, _ := channel.QueueDeclare(QueueName, false, false, false, false, nil)

	_ = channel.QueueBind(queue.Name, "delayMsg", ExchangeName, false, nil)
}

func main() {
	channel, _ := utils.GetRabbitMQChannel()

	headers := make(amqp.Table)
	headers["x-delay"] = 10000
	_ = channel.Publish(ExchangeName, "delayMsg", false, false,
		amqp.Publishing{
			Headers:     headers,
			ContentType: "text/plain",
			Body:        []byte("hello rabbit"),
		})
}
