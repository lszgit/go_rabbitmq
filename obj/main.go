package main

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"rabbit/utils"
)

const (
	ExchangeName = "exchange"
	QueueName    = "queue"
	RoutingKey   = "key"
)

func init() {
	channel, _ := utils.GetRabbitMQChannel()

	// 声明交换机
	err := channel.ExchangeDeclare(ExchangeName, "direct", false, false, false, false, nil)

	// 声明队列
	queue, err := channel.QueueDeclare(QueueName, false, false, false, false, nil)

	// 声明bind
	err = channel.QueueBind(queue.Name, RoutingKey, ExchangeName, false, nil)

	if err != nil {
		panic(err)
	}
}

type Student struct {
	Name string `json:"name"`
	Age  uint   `json:"age"`
}

func main() {
	channel, _ := utils.GetRabbitMQChannel()
	student := Student{
		Name: "lishuangzhi",
		Age:  18,
	}
	bytes, _ := json.Marshal(student)
	_ = channel.Publish(ExchangeName, RoutingKey, false, false, amqp.Publishing{
		ContentType: "text/plain",
		Body:        bytes,
	})

}
