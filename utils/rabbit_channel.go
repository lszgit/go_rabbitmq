package utils

import (
	"github.com/streadway/amqp"
)

func GetRabbitMQChannel() (*amqp.Channel, error) {
	conn, err := amqp.Dial("amqp://lsz:lsz@localhost:5672/")
	if err != nil {
		return nil, err
	}
	ch, err := conn.Channel()
	return ch, err
}
