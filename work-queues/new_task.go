package main

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"rabbit/utils"
)

const (
	QueueName = "hello"
)

func main() {
	ch, _ := utils.GetRabbitMQChannel()
	//开启发布确认
	ch.Confirm(true)
	q, _ := ch.QueueDeclare(
		QueueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	for i := 0; i < 20; i++ {
		msg := fmt.Sprintf("%d号消息", i+1)
		_ = ch.Publish(
			"",     // exchange
			q.Name, // routing key
			false,  // mandatory
			false,
			amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         []byte(msg),
			})
		log.Printf(" Sent %s", msg)
	}

}
