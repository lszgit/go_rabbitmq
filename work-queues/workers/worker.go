package main

import (
	"github.com/streadway/amqp"
	"log"
	"rabbit/utils"
	"sync"
)

func main() {
	ch, _ := utils.GetRabbitMQChannel()
	q, _ := ch.QueueDeclare(
		"hello", // name
		true,    // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	var wg sync.WaitGroup
	wg.Add(3)
	for i := 0; i < 3; i++ {
		go work(i+1, q, ch, &wg)
	}
	wg.Wait()
}

func work(id int, q amqp.Queue, ch *amqp.Channel, wg *sync.WaitGroup) {
	defer wg.Done()
	msgs, _ := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	forever := make(chan bool)
	go func() {
		for d := range msgs {
			log.Printf("[%d号worker]Received a message: %s", id, d.Body)
			d.Ack(false)
		}
	}()
	<-forever
}
